package com.credencys.dharashop;

import android.app.Application;
import android.content.Context;

import com.credencys.dharashop.utilities.CacheUtilities;

/**
 * Created by Dhara Shah on 03-08-2015.
 */
public class MyShopBagApp extends Application{
    private static MyShopBagApp mApp;
    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        setupCache();
    }

    /**
     * Sets up the cache to store data
     */
    private void setupCache() {
        CacheUtilities.init();
    }

    /**
     * returns the app context
     * @return
     */
    public static MyShopBagApp getAppContext(){
        if(mApp == null) {
            mApp = (MyShopBagApp)mContext;
        }
        return mApp;
    }
}
