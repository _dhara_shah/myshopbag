package com.credencys.dharashop.utilities;

import android.support.v4.util.LruCache;
import android.text.TextUtils;

import com.credencys.dharashop.MyShopBagApp;
import com.credencys.dharashop.db.DBHelper;
import com.credencys.dharashop.models.Product;
import com.credencys.dharashop.prefs.SharedPrefs;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Associated with operations related to the cache
 * Created by Dhara Shah on 04/08/2015.
 */
public class CacheUtilities {
    private static LruCache<String, Object> lruCache;
    private static final int MAX_SIZE = 1024 * 1024 * 8;

    /**
     * Initializes Cache
     */
    public static void init(){
        lruCache = new LruCache<String, Object>(MAX_SIZE);
    }

    /**
     * Returns an instance of the cache
     * @return
     */
    private static LruCache<String, Object> getInstance(){
        if(lruCache == null) {
            init();
        }
        return lruCache;
    }

    /**
     * Stores the object in the cache
     * @param object
     */
    public static void storeDataIntoCache(Object object) {
        lruCache = getInstance();
        if(object instanceof HashMap<?, ?>) {
            lruCache.put(GlobalConsts.CACHE_DATA, (HashMap<String, Product>) object);
        }
    }

    /**
     * Gets the object from the cache
     * @param whatToGet
     * @return
     */
    public static Object getDataFromCache(String whatToGet){
        lruCache = getInstance();
        if(whatToGet.equals(GlobalConsts.CACHE_DATA)) {
            Object obj = lruCache.get(GlobalConsts.CACHE_DATA);
            if(obj!= null && obj instanceof HashMap<?,?>) {
                return (HashMap<String, Product>)obj;
            }else {
                return null;
            }
        }else {
            return null;
        }
    }

    /**
     * Updates the product quantity and total amount of the product already in the cache </br>
     * If the product is a new item, it is added to the list
     * @param product
     */
    public static void addToCart(Product product) {
        Product productObj = null;
        HashMap<String, Product> hashMap =
                (HashMap<String, Product>)getDataFromCache(GlobalConsts.CACHE_DATA);
        if(hashMap != null && hashMap.containsKey(product.getProductId())) {
            // get the list of items here
            productObj = hashMap.get(product.getProductId());
        }else {
            if(hashMap ==  null) {
                hashMap = new HashMap<>();
            }
            productObj = product;
        }

        String qty = productObj.getProductQty();
        String price = TextUtils.isEmpty(productObj.getProductDiscountPrice()) ?
                productObj.getProductPrice() : productObj.getProductDiscountPrice();

        int iQty = TextUtils.isEmpty(qty) ? 0 : Integer.parseInt(qty);
        long lPrice = Long.parseLong(price);

        productObj.setProductQty(String.valueOf((iQty + 1)));
        productObj.setTotalAmt(String.valueOf((lPrice * Integer.parseInt(productObj.getProductQty()))));

        hashMap.put(productObj.getProductId(), productObj);
        storeDataIntoCache(hashMap);

        // if auto sync is true
        if(SharedPrefs.getBoolean(SharedPrefs.AUTO_SYNC_ON, true)) {
            storeIntoTheDatabase(product);
        }
    }

    /**
     * Inserts or updates a product item in the database
     * @param product
     */
    private static void storeIntoTheDatabase(Product product){
        DBHelper dbHelper = DBHelper.getInstance(MyShopBagApp.getAppContext());
        dbHelper.insertOrUpdateCart(product, GlobalConsts.ADD_TO_CART);
    }

    /**
     * If sync is on the count is returned from the database
     * else the count is returned from cache
     * @return
     */
    public static int getItemCount() {
        if(SharedPrefs.getBoolean(SharedPrefs.AUTO_SYNC_ON, true)) {
            DBHelper dbHelper = DBHelper.getInstance(MyShopBagApp.getAppContext());
            int count = dbHelper.getItemCount();
            return count;
        }else {
            HashMap<String, Product> hashMap =
                    (HashMap<String, Product>)getDataFromCache(GlobalConsts.CACHE_DATA);
            return (hashMap  == null) ? 0  : hashMap.size();
        }
    }

    /**
     * Removes an item from the cart
     * @param product
     */
    public static void removeItemFromCart(Product product){
        HashMap<String, Product> hashMap =
                (HashMap<String, Product>)getDataFromCache(GlobalConsts.CACHE_DATA);
        if(hashMap != null && hashMap.containsKey(product.getProductId())) {
            hashMap.remove(product.getProductId());
        }
    }

    /**
     * Updates the database
     */
    public static void synchronizeWithDB(){
        List<Product> products = new ArrayList<>();
        HashMap<String, Product> hashMap =
                (HashMap<String, Product>)getDataFromCache(GlobalConsts.CACHE_DATA);
        if(hashMap != null) {
            for(String key : hashMap.keySet()){
                products.add(hashMap.get(key));
            }
        }
        DBHelper dbHelper = DBHelper.getInstance(MyShopBagApp.getAppContext());
        dbHelper.updateDB(products);
    }

    public static void clearCart() {
        HashMap<String, Product> hashMap = new HashMap<>();
        storeDataIntoCache(hashMap);

        if(SharedPrefs.getBoolean(SharedPrefs.AUTO_SYNC_ON,true)) {
            // delete from db  also
            DBHelper  dbHelper =DBHelper.getInstance(MyShopBagApp.getAppContext());
            dbHelper.clearCart();
        }
    }

    /**
     * Checks if the item is present in the cart <br/>
     * Assuming that we can only add one item at a time
     * @param product
     * @return
     */
    public static boolean isItemPresent(Product product) {
        HashMap<String, Product> hashMap =
                (HashMap<String, Product>)getDataFromCache(GlobalConsts.CACHE_DATA);
        if(hashMap != null && hashMap.containsKey(product.getProductId())) {
            return true;
        }else {
            return false;
        }
    }
}
