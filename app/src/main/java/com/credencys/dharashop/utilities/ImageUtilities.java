package com.credencys.dharashop.utilities;

import android.graphics.drawable.Drawable;

import com.credencys.dharashop.MyShopBagApp;

import java.io.IOException;
import java.io.InputStream;

/**
 * Utils related with the images
 * Created by Dahra Shah on 05/08/2015.
 */
public class ImageUtilities {
    /**
     * Returns the drawable with the specified image name
     * from the assets folder
     * @param imageName
     * @return
     */
    public static Drawable getDrawable(String imageName) {
        // load image
        try {
            // get input stream
            InputStream ims = MyShopBagApp.getAppContext().getAssets().open("images/" + imageName);
            // load image as Drawable
            Drawable d = Drawable.createFromStream(ims, null);
            // return the drawable
            return d;
        }
        catch(IOException ex) {
            return null;
        }
    }
}
