package com.credencys.dharashop.utilities;

/**
 * Holds all the constants used in the app
 * Created by Dhara Shah on 04/08/2015.
 */
public class GlobalConsts {
    public static final String PRODUCT="product";
    public static final String CACHE_DATA="cached_data";
    public static final String ACTION_CLEAR_CART="com.credencys.dhara.clearcart";
    public static final int ADD_TO_CART=101;
    public static final int REMOVE_FROM_CART=102;
    public static final int DELETE=103;
}
