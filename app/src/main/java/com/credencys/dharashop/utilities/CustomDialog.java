package com.credencys.dharashop.utilities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.credencys.dharashop.R;
import com.credencys.dharashop.prefs.SharedPrefs;

/**
 * Generates a custom dialog
 * Created by Dhara Shah on 05-08-2015.
 */
public class CustomDialog {
    private static boolean isClearCart;
    public interface ClearCartListener {
        void onClearCart();
    }

    /**
     * Shows the settings dialog
     * @param context
     * @param clearCartListener
     * @return
     */
    public static Dialog showSettingsDialog(Context context, final ClearCartListener clearCartListener) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        dialog.setContentView(R.layout.layout_dialog);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final TextView clearCart = (TextView) dialog.findViewById(R.id.txtClearCart);
        clearCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isClearCart = true;
                dialog.dismiss();
            }
        });

        if(SharedPrefs.getBoolean(SharedPrefs.AUTO_SYNC_ON, true)) {
            clearCart.setVisibility(View.VISIBLE);
        }else {
            clearCart.setVisibility(View.GONE);
        }

        CheckBox cbAutoSync =  (CheckBox)dialog.findViewById(R.id.checkAutoSync);
        cbAutoSync.setChecked(SharedPrefs.getBoolean(SharedPrefs.AUTO_SYNC_ON,true));

        cbAutoSync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                SharedPrefs.putBoolean(SharedPrefs.AUTO_SYNC_ON, isChecked);
                isClearCart = false;
                if(isChecked) {
                    // if checked then sync with  db
                    // else do nothing
                    CacheUtilities.synchronizeWithDB();
                    clearCart.setVisibility(View.VISIBLE);
                }else {
                    clearCart.setVisibility(View.GONE);
                }
            }
        });

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                // clear the cart only if the user chooses to do so
                if(isClearCart) {
                    if (clearCartListener != null) {
                        clearCartListener.onClearCart();
                    }
                }
            }
        });

        dialog.show();
        return dialog;
    }

}
