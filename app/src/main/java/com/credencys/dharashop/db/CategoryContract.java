package com.credencys.dharashop.db;

import android.provider.BaseColumns;

/**
 * Refers to the cateogry_master table
 */
public final class CategoryContract {
	public CategoryContract(){}
	
	public static abstract class CategoryEntry implements BaseColumns {
		public static final String TABLE_NAME = "category_master";
        public static final String COLUMN_NAME_CATEGORY_ID = "category_id";
        public static final String COLUMN_NAME_CATEGORY_NAME = "category_name";
	}
}
