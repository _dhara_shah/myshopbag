package com.credencys.dharashop.db;

import android.provider.BaseColumns;

/**
 * Refers to the cart_master Table
 * Created by Dhara Shah on 03-08-2015.
 */
public final class CartContract {
    public CartContract(){}

    public static abstract class CartEntry implements BaseColumns {
        public static final String TABLE_NAME="cart_master";
        public static final String COLUMN_NAME_PRODUCT_ID="product_id";
        public static final String COLUMN_NAME_QTY="product_qty";
        public static final String COLUMN_NAME_TOTAL = "total_amount";
    }
}
