package com.credencys.dharashop.db;

import android.provider.BaseColumns;

/**
 * Refers to the product_master table
 * Created by Dhara Shah on 03-08-2015.
 */
public final class ProductContract {
    public ProductContract(){}

    public static abstract class ProductEntry implements BaseColumns {
        public static final String TABLE_NAME = "product_master";
        public static final String COLUMN_NAME_PRODUCT_ID="product_id";
        public static final String COLUMN_NAME_PROD_CAT_ID = "category_id";
        public static final String COLUMN_NAME_PRODUCT_NAME = "product_name";
        public static final String COLUMN_NAME_PRODUCT_DESC="product_desc";
        public static final String COLUMN_NAME_PROD_IMAGE="product_image";
        public static final String COLUMN_NAME_PRODUCT_PRICE =  "product_price";
        public static final String COLUMN_NAME_PRODUCT_DISC_PRICE="product_disc_price";
        public static final String COLUMN_NAME_QTY="stock_level";
    }
}
