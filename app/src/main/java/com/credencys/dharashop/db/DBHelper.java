package com.credencys.dharashop.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.credencys.dharashop.models.Product;
import com.credencys.dharashop.utilities.GlobalConsts;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class DBHelper  extends SQLiteOpenHelper{
	private static DBHelper mDatabaseHelper;
	private static SQLiteDatabase mSqliteDb;
	private Context mContext;

	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "my_shop_bag.sqlite";

    /**
     * Gets an instance of the helper class
     * @param context
     * @return
     */
	public static DBHelper getInstance(Context context) {
		if (mDatabaseHelper == null) {
			mDatabaseHelper = new DBHelper(context);
			if(mSqliteDb != null) {
				mSqliteDb.close();
			}
		}
		return mDatabaseHelper;
	}

    /**
     * Initializes the database used
     * @param context
     */
	public DBHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		mContext = context;
		openDatabase();
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
        // do nothing
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		onUpgrade(db, oldVersion, newVersion);
	}
	
	/**
	 * Checks if the database is open or not
	 * @return
	 */
	@Override
	public synchronized void close() {
        if(mSqliteDb != null) {
            mSqliteDb.close();
        }
        super.close();
	}

	/**
	 * Open an existing database or
	 * copy the database from assets if it doesnot exist
	 * @return
	 */
	public SQLiteDatabase openDatabase() {
		File databaseFile = new File("/data/data/"+
				mContext.getApplicationInfo().packageName + "/databases");
		File dbFile = mContext.getDatabasePath(DATABASE_NAME);

		if (!databaseFile.exists()){
			databaseFile.mkdir();
		}

		if (!dbFile.exists()) {
			try {
				this.getReadableDatabase();
				copyDatabase(dbFile);
			} catch (IOException e) {
				throw new RuntimeException("Error creating source database", e);
			}
		}

		mSqliteDb = SQLiteDatabase.openDatabase(dbFile.getPath(), null,
				SQLiteDatabase.NO_LOCALIZED_COLLATORS|SQLiteDatabase.OPEN_READONLY|SQLiteDatabase.CREATE_IF_NECESSARY);
		return mSqliteDb;
	}

	/**
	 * Copy database from assets folder into the memory
	 * @param dbFile
	 * @throws IOException
	 */
	private void copyDatabase(File dbFile) throws IOException {
		InputStream is = mContext.getAssets().open(DATABASE_NAME);
		OutputStream os = new FileOutputStream(dbFile);

		byte[] buffer = new byte[1024];
		while (is.read(buffer) > 0) {
			os.write(buffer);
		}

		os.flush();
		os.close();
		is.close();
	}

	/**
	 * Gets the list of products under all the categories
	 * @return
	 */
	public Cursor getProductsUnderAllCategories() {
        String sql = " select " +
                ProductContract.ProductEntry.TABLE_NAME + "." + ProductContract.ProductEntry._ID + "," +
                ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_ID + "," +
                ProductContract.ProductEntry.TABLE_NAME + "."  + ProductContract.ProductEntry.COLUMN_NAME_PROD_CAT_ID + "," +
                ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_NAME + "," +
                ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_DESC + "," +
                ProductContract.ProductEntry.COLUMN_NAME_PROD_IMAGE + "," +
                ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_PRICE + "," +
                ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_DISC_PRICE + "," +
                CategoryContract.CategoryEntry.COLUMN_NAME_CATEGORY_NAME +
                " from " +
                ProductContract.ProductEntry.TABLE_NAME + " , " + CategoryContract.CategoryEntry.TABLE_NAME +
                " where " +
                ProductContract.ProductEntry.TABLE_NAME + "." + ProductContract.ProductEntry.COLUMN_NAME_PROD_CAT_ID +
                "=" +
                CategoryContract.CategoryEntry.TABLE_NAME + "." + CategoryContract.CategoryEntry.COLUMN_NAME_CATEGORY_ID;

		mSqliteDb = this.getReadableDatabase();
        Cursor c = mSqliteDb.rawQuery(sql, null);
		return c;
    }

	/**
	 * Get the number of items in the basket
	 * @return
	 */
	public int getItemCount() {
        int counter = 0;
        mSqliteDb = this.openDatabase();
        Cursor c = mSqliteDb.query(CartContract.CartEntry.TABLE_NAME,
                new String[]{CartContract.CartEntry._ID},
                null,
                null,
                null,
                null,
                null);
        if(c.moveToFirst()) {
            counter = c.getCount();
        }
        c.close();
        return counter;
	}

    /**
     * Insert an item into the database
     * or else update the quantity of the product in the cart
     * @param product
     * @param action
     */
	public void insertOrUpdateCart(Product product, int action) {
		int qty = 0;
		long price = 0;
		long totalPrice = 0;
		mSqliteDb = this.getWritableDatabase();
		Cursor c = mSqliteDb.query(CartContract.CartEntry.TABLE_NAME,
				null,
				CartContract.CartEntry.COLUMN_NAME_PRODUCT_ID + " = ?",
				new String[]{product.getProductId()},
				null,
				null,
				null);

		if(c.moveToFirst()) {
			String quantity = c.getString(c.getColumnIndex(CartContract.CartEntry.COLUMN_NAME_QTY));
			String strPrice = c.getString(c.getColumnIndex(CartContract.CartEntry.COLUMN_NAME_TOTAL));
			String strTotalPrice = c.getString(c.getColumnIndex(CartContract.CartEntry.COLUMN_NAME_TOTAL));
			price = Long.parseLong(strPrice);
			totalPrice = Long.parseLong(strTotalPrice);

			switch (action) {
				case GlobalConsts.ADD_TO_CART:
					qty = Integer.parseInt(quantity) + 1;
					totalPrice += price;
					break;

				case GlobalConsts.REMOVE_FROM_CART:
					qty = Integer.parseInt(quantity) - 1;
					totalPrice -= price;
					break;

				case GlobalConsts.DELETE:
					qty = 0;
					totalPrice = 0;
					break;

				default:
					break;
			}

			if(qty == 0) {
				// delete item from list
				String sql = "delete from " +
						CartContract.CartEntry.TABLE_NAME +
						" where " +
						CartContract.CartEntry.COLUMN_NAME_PRODUCT_ID + "='" + product.getProductId() + "'";
				mSqliteDb.execSQL(sql);
			}else {
				ContentValues values = new ContentValues();
				values.put(CartContract.CartEntry.COLUMN_NAME_QTY, String.valueOf(qty));
				values.put(CartContract.CartEntry.COLUMN_NAME_TOTAL, String.valueOf(totalPrice));

				mSqliteDb.update(CartContract.CartEntry.TABLE_NAME,
						values,
						CartContract.CartEntry.COLUMN_NAME_PRODUCT_ID + "= ?",
						new String[]{product.getProductId()});
			}

		}else  {
            // insert into the database
            ContentValues values = new ContentValues();
            values.put(CartContract.CartEntry.COLUMN_NAME_QTY,product.getProductQty());
            values.put(CartContract.CartEntry.COLUMN_NAME_TOTAL, product.getTotalAmt());
            values.put(CartContract.CartEntry.COLUMN_NAME_PRODUCT_ID, product.getProductId());

            mSqliteDb.insert(CartContract.CartEntry.TABLE_NAME,
                    null,
                    values);
        }

		c.close();
	}

    /**
     * Gets the list of products in the cart
     * @return
     */
	public List<Product> getItemsInCart() {
		try {
            List<Product> productList = new ArrayList<>();
            mSqliteDb = this.openDatabase();

            String sql = " select " +
                    CartContract.CartEntry.TABLE_NAME + "." + CartContract.CartEntry._ID + "," +
                    CartContract.CartEntry.TABLE_NAME + "." + CartContract.CartEntry.COLUMN_NAME_PRODUCT_ID  + "," +
                    CartContract.CartEntry.COLUMN_NAME_QTY + "," +
                    CartContract.CartEntry.COLUMN_NAME_TOTAL + ","  +
                    ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_NAME  + "," +
                    ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_DISC_PRICE + "," +
                    ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_DESC + "," +
                    ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_PRICE + "," +
                    ProductContract.ProductEntry.COLUMN_NAME_PROD_IMAGE + "," +
                    ProductContract.ProductEntry.TABLE_NAME  +  "." + ProductContract.ProductEntry.COLUMN_NAME_PROD_CAT_ID + "," +
                    CategoryContract.CategoryEntry.COLUMN_NAME_CATEGORY_NAME +
                    " from " + CartContract.CartEntry.TABLE_NAME  + "  join "  +
                    ProductContract.ProductEntry.TABLE_NAME + " on " +
                    CartContract.CartEntry.TABLE_NAME +  "." + CartContract.CartEntry.COLUMN_NAME_PRODUCT_ID  +  "=" +
                    ProductContract.ProductEntry.TABLE_NAME + "." + ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_ID +
                    " join " +
                    CategoryContract.CategoryEntry.TABLE_NAME + " on  " +
                    ProductContract.ProductEntry.TABLE_NAME + "." + ProductContract.ProductEntry.COLUMN_NAME_PROD_CAT_ID + "=" +
                    CategoryContract.CategoryEntry.TABLE_NAME +  "."  + CategoryContract.CategoryEntry.COLUMN_NAME_CATEGORY_ID;

            Cursor c = mSqliteDb.rawQuery(sql, null);
            if(c !=  null && c.moveToFirst()) {
                do {
                    Product product = new Product();
                    product.setCategoryId(c.getString(c.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PROD_CAT_ID)));
                    product.setCategoryName(c.getString(c.getColumnIndex(CategoryContract.CategoryEntry.COLUMN_NAME_CATEGORY_NAME)));
                    product.setProductDesc(c.getString(c.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_DESC)));
                    product.setProductDiscountPrice(c.getString(c.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_DISC_PRICE)));
                    product.setProductId(c.getString(c.getColumnIndex(CartContract.CartEntry.COLUMN_NAME_PRODUCT_ID)));
                    product.setProductImage(c.getString(c.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PROD_IMAGE)));
                    product.setProductPrice(c.getString(c.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_PRICE)));
                    product.setProductQty(c.getString(c.getColumnIndex(CartContract.CartEntry.COLUMN_NAME_QTY)));
                    product.setTotalAmt(c.getString(c.getColumnIndex(CartContract.CartEntry.COLUMN_NAME_TOTAL)));
                    product.setProductName(c.getString(c.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_NAME)));
                    productList.add(product);
                }while(c.moveToNext());
            }
            c.close();
            return productList;
        }finally {
            if(mSqliteDb.isOpen()){
                mSqliteDb.close();
            }
        }
	}

    /**
     * update the db with the product items
     * @param products
     */
	public void updateDB(List<Product> products) {
		try {
            mSqliteDb = this.getWritableDatabase();

            // FIXME: I am being deleted all the time, when there is a change in the cart
            // FIXME: and sync option is set to true from false.
            // FIXME: Only those items that have been modified or new
            // FIXME: added need to be inserted or updated

            //  delete all the old records and  insert afresh
            mSqliteDb.delete(CartContract.CartEntry.TABLE_NAME, null, null);

            for(Product product : products) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(CartContract.CartEntry.COLUMN_NAME_PRODUCT_ID,  product.getProductId());
                contentValues.put(CartContract.CartEntry.COLUMN_NAME_QTY,  product.getProductQty());
                contentValues.put(CartContract.CartEntry.COLUMN_NAME_TOTAL,  product.getTotalAmt());
                mSqliteDb.insert(CartContract.CartEntry.TABLE_NAME, null,  contentValues);
            }
        }finally {
            if(mSqliteDb.isOpen()) {
                mSqliteDb.close();
            }
        }
	}

    /**
     * Clear the items in the database, auto sync is on
     */
	public void clearCart(){
		try {
            mSqliteDb = this.getWritableDatabase();
            //  delete all the old records and  insert afresh
            mSqliteDb.delete(CartContract.CartEntry.TABLE_NAME,  null,null);
        }finally {
            if(mSqliteDb.isOpen()) {
                mSqliteDb.close();
            }
        }
	}
}
