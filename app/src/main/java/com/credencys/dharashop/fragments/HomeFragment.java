package com.credencys.dharashop.fragments;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.credencys.dharashop.MyShopBagApp;
import com.credencys.dharashop.R;
import com.credencys.dharashop.activities.DetailActivity;
import com.credencys.dharashop.adapters.ProductAdapter;
import com.credencys.dharashop.asynctasks.GetDataAsyncTask;
import com.credencys.dharashop.db.CategoryContract;
import com.credencys.dharashop.db.DBHelper;
import com.credencys.dharashop.db.ProductContract;
import com.credencys.dharashop.interfaces.ResponseListener;
import com.credencys.dharashop.models.Product;
import com.credencys.dharashop.utilities.CacheUtilities;
import com.credencys.dharashop.utilities.GlobalConsts;

/**
 * Created by USER on 04-08-2015.
 */
public class HomeFragment extends BaseFragment implements AdapterView.OnItemClickListener, ResponseListener {
    private static HomeFragment mFragment;
    private ListView mLstView;
    private View mView;
    private DBHelper mDbHelper;
    private Cursor mCursor;
    private ProductAdapter mProductAdapter;
    private ResponseListener mListener;

    /**
     * Creates and returns an instance of {@link HomeFragment}
     * @return
     */
    public static HomeFragment newInstance() {
        mFragment = new HomeFragment();
        return mFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_home, container, false);
        mDbHelper = DBHelper.getInstance(MyShopBagApp.getAppContext());
        mLstView = (ListView)mView.findViewById(android.R.id.list);
        mLstView.setOnItemClickListener(this);
        mListener = this;
        setupActionBar(mView, getActivity());
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        new GetDataAsyncTask(mListener).execute();
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(MyShopBagApp.getAppContext().getString(R.string.app_name));
        mTxtItemCount.setText(""+CacheUtilities.getItemCount());
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // lead to the detail view
        Product product = new Product();
        Cursor c = mProductAdapter.getCursor();
        c.moveToPosition(position);

        product.setCategoryName(c.getString(c.getColumnIndex(CategoryContract.CategoryEntry.COLUMN_NAME_CATEGORY_NAME)));
        product.setProductName(c.getString(c.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_NAME)));
        product.setProductDesc(c.getString(c.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_DESC)));
        product.setProductDiscountPrice(c.getString(c.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_DISC_PRICE)));
        product.setProductId(c.getString(c.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_ID)));
        product.setProductImage(c.getString(c.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PROD_IMAGE)));
        product.setProductPrice(c.getString(c.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_PRICE)));
        product.setCategoryId(c.getString(c.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PROD_CAT_ID)));

        Intent intent = new Intent(MyShopBagApp.getAppContext(),
                DetailActivity.class);
        intent.putExtra(GlobalConsts.PRODUCT, product);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onResponseReceived(Cursor cursor) {
        mCursor = cursor;
        mProductAdapter = new ProductAdapter(MyShopBagApp.getAppContext(),
                mCursor, false);
        mLstView.setAdapter(mProductAdapter);
    }
}
