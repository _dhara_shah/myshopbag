package com.credencys.dharashop.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.credencys.dharashop.MyShopBagApp;
import com.credencys.dharashop.R;
import com.credencys.dharashop.activities.CartActivity;
import com.credencys.dharashop.utilities.CacheUtilities;
import com.credencys.dharashop.utilities.CustomDialog;
import com.credencys.dharashop.utilities.GlobalConsts;

/**
 * Created by Dhara Shah on 04-08-2015.
 */
public class BaseFragment extends Fragment implements CustomDialog.ClearCartListener{
    private Toolbar mToolBar;
    private TextView mTxtHeader;
    public ImageView mImgBack;
    private ImageView mImgSettings;
    public TextView mTxtItemCount;
    private RelativeLayout mRelCart;
    private FragmentActivity mFragmentActivity;
    private CustomDialog.ClearCartListener mClearCartListener;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mClearCartListener = this;
    }

    /**
     * Sets up the toolbar that would be visible on all the screens
     * @param view
     */
    private void setupToolBar(View view) {
        mToolBar = (Toolbar) view.findViewById(R.id.toolbar);
        mTxtHeader = (TextView) view.findViewById(R.id.txtHeader);
        mImgBack = (ImageView) view.findViewById(R.id.imgBack);
        mTxtItemCount = (TextView)view.findViewById(R.id.txtItemCount);
        mRelCart = (RelativeLayout)view.findViewById(R.id.relCart);
        mImgSettings = (ImageView)view.findViewById(R.id.imgSettings);
        mImgBack.setVisibility(View.GONE);

        mRelCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!(mFragmentActivity instanceof CartActivity)) {
                    Intent intent = new Intent(MyShopBagApp.getAppContext(),
                            CartActivity.class);
                    startActivity(intent);
                }
            }
        });

        mImgSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomDialog.showSettingsDialog(getActivity(), mClearCartListener);
            }
        });
    }

    /**
     * Sets the title of the screen visible to the user
     * @param header
     */
    public void setTitle(String header) {
        mTxtHeader.setText(header);
    }

    /**
     * Set the toolbar as the actionbar
     * @param view
     * @param activity
     */
    public void setupActionBar(View view, FragmentActivity activity) {
        setupToolBar(view);
        ((AppCompatActivity) activity).setSupportActionBar(mToolBar);
        mFragmentActivity = activity;
        mToolBar.setNavigationIcon(null);
        mToolBar.setLogo(null);
    }

    /**
     *  clears the cart on user request
     *  once that is done, the item count gets updated
     *  if the current fragment is the {@link CartFragment}
     *  then the list is notified
     */
    @Override
    public void onClearCart() {
        CacheUtilities.clearCart();
        mTxtItemCount.setText(""+CacheUtilities.getItemCount());
        Intent intent =  new  Intent(GlobalConsts.ACTION_CLEAR_CART);
        getActivity().sendBroadcast(intent);
    }
}
