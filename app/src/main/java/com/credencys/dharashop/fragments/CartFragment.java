package com.credencys.dharashop.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.credencys.dharashop.MyShopBagApp;
import com.credencys.dharashop.R;
import com.credencys.dharashop.activities.HomescreenActivity;
import com.credencys.dharashop.adapters.CartAdapter;
import com.credencys.dharashop.db.DBHelper;
import com.credencys.dharashop.models.Product;
import com.credencys.dharashop.prefs.SharedPrefs;
import com.credencys.dharashop.utilities.CacheUtilities;
import com.credencys.dharashop.utilities.GlobalConsts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Dhara Shah on 04/08/2015.
 */
public class CartFragment extends BaseFragment implements View.OnClickListener {
    private static CartFragment mFragment;
    private View mView;
    private CartAdapter mCartAdapter;
    private List<Product> mProductList;
    private Button mBtnCheckout;
    private Button mBtnContinueShopping;
    private TextView mTxtTotalAmt;
    private ListView mLstView;
    private DBHelper mDbHelper;
    private BroadcastReceiver mReceiver;

    /**
     * Creates and returns an instance of the fragment
     * @return
     */
    public static CartFragment newInstance() {
        mFragment = new CartFragment();
        return mFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_cart, container, false);
        setupActionBar(mView,getActivity());

        mTxtTotalAmt =  (TextView)mView.findViewById(R.id.txtTotalAmount);
        mBtnContinueShopping = (Button)mView.findViewById(R.id.btnContinueShopping);
        mBtnCheckout = (Button)mView.findViewById(R.id.btnCheckout);
        mLstView = (ListView)mView.findViewById(android.R.id.list);
        mBtnCheckout.setOnClickListener(this);
        mBtnContinueShopping.setOnClickListener(this);

        mImgBack.setVisibility(View.VISIBLE);
        mImgBack.setOnClickListener(this);

        mProductList = new ArrayList<>();
        mDbHelper = DBHelper.getInstance(MyShopBagApp.getAppContext());
        return mView;
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(MyShopBagApp.getAppContext().getString(R.string.cart_page) +
                " ( " + CacheUtilities.getItemCount() + " ) ");
        mTxtItemCount.setText("" + CacheUtilities.getItemCount());

        mReceiver =  new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if(intent != null)  {
                    setAdapter();
                }
            }
        };
        getActivity().registerReceiver(mReceiver,
                new IntentFilter(GlobalConsts.ACTION_CLEAR_CART));
    }

    @Override
    public void onPause() {
        super.onPause();
        if(mReceiver != null) {
            getActivity().unregisterReceiver(mReceiver);
        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setAdapter();
    }

    /**
     * Sets the adapter, and displays the items in the cart
     */
    private void setAdapter() {
        if(SharedPrefs.getBoolean(SharedPrefs.AUTO_SYNC_ON, true)) {
            mProductList  =  mDbHelper.getItemsInCart();
        }else {
            HashMap<String, Product> hashMap =
                    (HashMap<String, Product>) CacheUtilities.getDataFromCache(GlobalConsts.CACHE_DATA);
            if(hashMap != null) {
                for(String key : hashMap.keySet()) {
                    mProductList.add(hashMap.get(key));
                }
            }
        }

        if(mProductList == null) {
            mProductList = new ArrayList<>();
        }

        mTxtItemCount.setText(""+mProductList.size());
        mCartAdapter = new CartAdapter(MyShopBagApp.getAppContext(),
                R.layout.individual_row, mProductList, mFragment);
        mLstView.setAdapter(mCartAdapter);
        updateTotalAmount();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCheckout:
                // do nothing
                break;

            case R.id.btnContinueShopping:
                Intent intent = new Intent(MyShopBagApp.getAppContext(),
                        HomescreenActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                break;

            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
        }
    }

    /**
     * Deletes a particular item from the cart as a whole <br/>
     * If sync is on, the item is deleted from the database and cache
     * else from the cache alone
     * @param product
     */
    public void deleteItemFromCart(Product product) {
        if(SharedPrefs.getBoolean(SharedPrefs.AUTO_SYNC_ON,true)) {
            mDbHelper.insertOrUpdateCart(product, GlobalConsts.DELETE);
            mDbHelper.close();
        }

        CacheUtilities.removeItemFromCart(product);
        mProductList.remove(product);
        mCartAdapter.notifyDataSetChanged();

        updateTotalAmount();
    }

    /**
     * Updates the total amount payable on item addition or deletion
     */
    private void updateTotalAmount()  {
        long totalAmt = 0;
        for(Product product : mProductList) {
            totalAmt += Long.parseLong(product.getTotalAmt());
        }
        mTxtTotalAmt.setText(MyShopBagApp.getAppContext().getString(R.string.rs) +
                "  " + totalAmt);

        setTitle(MyShopBagApp.getAppContext().getString(R.string.cart_page) +
                " ( " + CacheUtilities.getItemCount() + " ) ");

        mTxtItemCount.setText("" + CacheUtilities.getItemCount());
    }
}
