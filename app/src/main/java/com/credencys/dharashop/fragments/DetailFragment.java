package com.credencys.dharashop.fragments;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.credencys.dharashop.MyShopBagApp;
import com.credencys.dharashop.R;
import com.credencys.dharashop.activities.CartActivity;
import com.credencys.dharashop.models.Product;
import com.credencys.dharashop.utilities.CacheUtilities;
import com.credencys.dharashop.utilities.GlobalConsts;
import com.credencys.dharashop.utilities.ImageUtilities;

/**
 * Created by user on 04/08/2015.
 */
public class DetailFragment extends BaseFragment implements View.OnClickListener {
    private static DetailFragment mFragment;
    private View mView;
    private Product mProduct;
    private ImageView mImgProduct;
    private Button mBtnBuyNow;
    private Button mBtnAddToCart;
    private TextView mTxtProductName;
    private TextView mTxtProductDesc;
    private TextView mTxtProductPrice;
    private TextView mTxtProductDiscPrice;
    private static final int ACTION_ADD = 1001;
    private static final int ACTION_BUY_NOW = 1002;

    /**
     * Creates and returns an instance of {@link DetailFragment}
     * @param product
     * @return
     */
    public static DetailFragment newInstance(Product product) {
        mFragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putSerializable(GlobalConsts.PRODUCT, product);
        mFragment.setArguments(args);
        return mFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_detail, container, false);

        if(getArguments() != null) {
            mProduct = (Product)getArguments().getSerializable(GlobalConsts.PRODUCT);
        }

        setupActionBar(mView, getActivity());

        mBtnBuyNow = (Button)mView.findViewById(R.id.btnBuyNow);
        mBtnAddToCart = (Button)mView.findViewById(R.id.btnAddToCart);
        mTxtProductName = (TextView)mView.findViewById(R.id.txtProductName);
        mTxtProductDesc = (TextView)mView.findViewById(R.id.txtDesc);
        mTxtProductPrice = (TextView)mView.findViewById(R.id.txtOriginalPrice);
        mTxtProductDiscPrice = (TextView)mView.findViewById(R.id.txtDiscPrice);
        mImgProduct = (ImageView)mView.findViewById(R.id.imgProduct);

        mImgBack.setVisibility(View.VISIBLE);

        mImgBack.setOnClickListener(this);
        mBtnBuyNow.setOnClickListener(this);
        mBtnAddToCart.setOnClickListener(this);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        // set data here
        mTxtProductDesc.setText(mProduct.getProductDesc());
        mTxtProductName.setText(mProduct.getProductName());
        mTxtProductPrice.setText(MyShopBagApp.getAppContext().getString(R.string.rs) + " " + mProduct.getProductPrice());

        if(TextUtils.isEmpty(mProduct.getProductDiscountPrice())) {
            mTxtProductDiscPrice.setVisibility(View.GONE);
            mTxtProductPrice.setPaintFlags(mTxtProductPrice.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
        }else {
            mTxtProductDiscPrice.setVisibility(View.VISIBLE);
            mTxtProductDiscPrice.setText(MyShopBagApp.getAppContext().getString(R.string.rs) +
                    " " + mProduct.getProductDiscountPrice());
            mTxtProductPrice.setPaintFlags(mTxtProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        mImgProduct.setImageDrawable(ImageUtilities.getDrawable(mProduct.getProductImage()));
    }

    @Override
    public void onResume() {
        super.onResume();
        setTitle(mProduct.getProductName());
        mTxtItemCount.setText("" + CacheUtilities.getItemCount());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnAddToCart:
                addToCart(ACTION_ADD);
                break;

            case R.id.btnBuyNow:
                addToCart(ACTION_BUY_NOW);

                // lead to the cart page
                Intent intent  = new Intent(MyShopBagApp.getAppContext(),
                        CartActivity.class);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                break;

            case R.id.imgBack:
                getActivity().onBackPressed();
                break;
        }
    }

    /**
     * To item to the cache
     */
    private void addToCart(int action) {
        if(CacheUtilities.isItemPresent(mProduct)) {
            if(action == ACTION_ADD) {
                Toast.makeText(MyShopBagApp.getAppContext(),
                        MyShopBagApp.getAppContext().getString(R.string.item_already_added),
                        Toast.LENGTH_SHORT).show();
                return;
            }
        }else {
            // add to the cart and update the count
            CacheUtilities.addToCart(mProduct);
            mTxtItemCount.setText(""+CacheUtilities.getItemCount());
            if(action == ACTION_ADD) {
                Toast.makeText(MyShopBagApp.getAppContext(),
                        MyShopBagApp.getAppContext().getString(R.string.item_added),
                        Toast.LENGTH_SHORT).show();
            }
        }
    }
}
