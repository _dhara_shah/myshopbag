package com.credencys.dharashop.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.credencys.dharashop.MyShopBagApp;
import com.credencys.dharashop.R;
import com.credencys.dharashop.db.DBHelper;
import com.credencys.dharashop.db.ProductContract;
import com.credencys.dharashop.fragments.CartFragment;
import com.credencys.dharashop.models.Product;
import com.credencys.dharashop.utilities.ImageUtilities;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Loads the items in the shopping cart
 * Created by Dhara Shah on 04/08/2015.
 */
public class CartAdapter extends ArrayAdapter<Product>{
    private int RESOURCE;
    private Context mContext;
    private DBHelper mDBHelper;
    private List<Product> mProductList;
    private Fragment mFragment;
    private int currentPosition;
    private int lastPosition;

    public CartAdapter(Context context, int resource,
                       List<Product> objects, Fragment fragment) {
        super(context, resource, objects);
        mContext = context;
        RESOURCE = resource;
        mProductList = objects;
        mFragment = fragment;
        mDBHelper = DBHelper.getInstance(MyShopBagApp.getAppContext());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder vh = null;
        if(view == null) {
            LayoutInflater inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(RESOURCE, parent, false);
            vh = new ViewHolder();
            vh.txtProductName = (TextView)view.findViewById(R.id.txtProductName);
            vh.txtProductPrice = (TextView)view.findViewById(R.id.txtProductPrice);
            vh.imgDelete = (ImageView)view.findViewById(R.id.imgDelete);
            vh.txtTotalAmt =  (TextView)view.findViewById(R.id.txtTotalAmt);
            vh.imgProduct = (ImageView)view.findViewById(R.id.imgProduct);
            vh.txtQty = (TextView)view.findViewById(R.id.txtQty);
            view.setTag(vh);
        }else {
            vh = (ViewHolder)view.getTag();
        }

        // list item view animation
        currentPosition = position;
        Animation animation = AnimationUtils.loadAnimation(mContext,
                (currentPosition > lastPosition) ? R.anim.up_from_bottom : R.anim.down_from_top);
        view.startAnimation(animation);
        lastPosition = currentPosition;

        Product product = mProductList.get(position);
        String price = TextUtils.isEmpty(product.getProductDiscountPrice()) ?
                product.getProductPrice() : product.getProductDiscountPrice();
        vh.txtProductPrice.setText(MyShopBagApp.getAppContext().getString(R.string.rs) + " "  + price);
        vh.txtProductName.setText(product.getProductName());
        vh.imgDelete.setTag(String.valueOf(position));
        vh.txtTotalAmt.setText("Total: " +
                MyShopBagApp.getAppContext().getString(R.string.rs) +
                " " +
                product.getTotalAmt());

        vh.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int pos = Integer.parseInt((String) ((ImageView) v).getTag());
                Product productObj = mProductList.get(pos);
                if (mFragment != null && mFragment instanceof CartFragment) {
                    ((CartFragment) mFragment).deleteItemFromCart(productObj);
                }
            }
        });
        vh.txtQty.setText(MyShopBagApp.getAppContext().getString(R.string.qty) + " " + product.getProductQty());
        vh.imgProduct.setImageDrawable(ImageUtilities.getDrawable(product.getProductImage()));
        return view;
    }

    static class ViewHolder {
        TextView txtProductName;
        TextView txtProductPrice;
        ImageView imgProduct;
        ImageView imgDelete;
        TextView txtTotalAmt;
        TextView txtQty;
    }
}
