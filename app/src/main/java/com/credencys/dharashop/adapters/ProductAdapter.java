package com.credencys.dharashop.adapters;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Paint;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.CursorAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.credencys.dharashop.MyShopBagApp;
import com.credencys.dharashop.R;
import com.credencys.dharashop.db.ProductContract;
import com.credencys.dharashop.utilities.ImageUtilities;

/**
 * Loads items into the home screen list
 * Created by Dhara Shah on 04/08/2015.
 */
public class ProductAdapter extends CursorAdapter{
    private int lastId;
    private int currentId;
    private Context mContext;
    public ProductAdapter(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
        mContext = context;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.individual_item_row, parent, false);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        ImageView imgProduct = (ImageView)view.findViewById(R.id.imgProduct);
        TextView txtProductName = (TextView)view.findViewById(R.id.txtProductName);
        TextView txtProductPrice = (TextView)view.findViewById(R.id.txtProductPrice);
        TextView txtProductDiscountPrice = (TextView)view.findViewById(R.id.txtProductDiscPrice);

        // list item view animation
        currentId = cursor.getInt(cursor.getColumnIndex(ProductContract.ProductEntry._ID));
        Animation animation = AnimationUtils.loadAnimation(mContext,
                (currentId > lastId) ? R.anim.up_from_bottom : R.anim.down_from_top);
        view.startAnimation(animation);
        lastId = currentId;

        String discountPrice = cursor.getString(cursor.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_DISC_PRICE));
        String productName = cursor.getString(cursor.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_NAME));
        String productPrice = cursor.getString(cursor.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PRODUCT_PRICE));

        txtProductPrice.setText(MyShopBagApp.getAppContext().getString(R.string.rs) + " " + productPrice);
        txtProductName.setText(productName);

        if(TextUtils.isEmpty(discountPrice)) {
            txtProductDiscountPrice.setVisibility(View.GONE);
            txtProductPrice.setPaintFlags(txtProductPrice.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
        }else {
            txtProductDiscountPrice.setVisibility(View.VISIBLE);
            txtProductDiscountPrice.setText(MyShopBagApp.getAppContext().getString(R.string.rs) + " " + discountPrice);
            txtProductPrice.setPaintFlags(txtProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }

        String imageName = cursor.getString(cursor.getColumnIndex(ProductContract.ProductEntry.COLUMN_NAME_PROD_IMAGE));
        imgProduct.setImageDrawable(ImageUtilities.getDrawable(imageName));
    }
}
