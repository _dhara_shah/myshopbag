package com.credencys.dharashop.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.credencys.dharashop.R;
import com.credencys.dharashop.fragments.DetailFragment;
import com.credencys.dharashop.models.Product;
import com.credencys.dharashop.utilities.GlobalConsts;

/**
 * Created by user on 04/08/2015.
 */
public class DetailActivity extends BaseActivity {
    private Product mProduct;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);

        if(getIntent().getExtras()!= null) {
            mProduct = (Product)getIntent().getSerializableExtra(GlobalConsts.PRODUCT);
        }

        // load the detail fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        DetailFragment fragment = DetailFragment.newInstance(mProduct);
        transaction.replace(R.id.frame_content, fragment);
        transaction.commit();
    }
}
