package com.credencys.dharashop.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.credencys.dharashop.MyShopBagApp;
import com.credencys.dharashop.R;

public class SplashActivity extends BaseActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable(){
            @Override
            public void run() {
                // lead to the home screen
                Intent intent = new Intent(MyShopBagApp.getAppContext(),
                        HomescreenActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                finish();
            }
        }, 1500);
    }
}
