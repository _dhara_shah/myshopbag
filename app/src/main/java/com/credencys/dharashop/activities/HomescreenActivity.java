package com.credencys.dharashop.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.credencys.dharashop.R;
import com.credencys.dharashop.fragments.HomeFragment;

/**
 * Created by USER on 03-08-2015.
 */
public class HomescreenActivity extends BaseActivity{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);

        // load the home fragment
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        HomeFragment fragment = HomeFragment.newInstance();
        ft.replace(R.id.frame_content,fragment);
        ft.commit();
    }
}
