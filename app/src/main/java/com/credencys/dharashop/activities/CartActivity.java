package com.credencys.dharashop.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.credencys.dharashop.R;
import com.credencys.dharashop.fragments.CartFragment;

/**
 * Created by user on 04/08/2015.
 */
public class CartActivity extends BaseActivity {
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_common);

        // load the cart fragment
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        CartFragment fragment = CartFragment.newInstance();
        ft.replace(R.id.frame_content, fragment);
        ft.commit();
    }
}
