package com.credencys.dharashop.interfaces;

import android.database.Cursor;

/**
 * Created by user on 04/08/2015.
 */
public interface ResponseListener {
    /**
     * Returns a response to the implementing function
     * @param cursor
     */
    void onResponseReceived(Cursor cursor);
}
