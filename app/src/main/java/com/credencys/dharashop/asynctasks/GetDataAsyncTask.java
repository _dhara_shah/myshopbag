package com.credencys.dharashop.asynctasks;

import android.database.Cursor;
import android.os.AsyncTask;

import com.credencys.dharashop.MyShopBagApp;
import com.credencys.dharashop.db.DBHelper;
import com.credencys.dharashop.interfaces.ResponseListener;

/**
 * Gets data from the database
 * Created by Dhara Shah on 04/08/2015.
 */
public class GetDataAsyncTask extends AsyncTask<Void,  Void, Cursor>{
    private DBHelper mDbHelper;
    private ResponseListener mListener;

    public GetDataAsyncTask(ResponseListener listener) {
        mListener = listener;
        mDbHelper = DBHelper.getInstance(MyShopBagApp.getAppContext());
    }

    @Override
    protected Cursor doInBackground(Void... params) {
        Cursor c = mDbHelper.getProductsUnderAllCategories();
        return c;
    }

    @Override
    protected void onPostExecute(Cursor cursor) {
        super.onPostExecute(cursor);
        if(mListener != null) {
            mListener.onResponseReceived(cursor);
        }
    }
}
